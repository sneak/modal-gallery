/*
	Modal Gallery
	V1.04
	By @sneak
	https://gitlab.com/sneak/modal-gallery

	@preserve
 */

var ModalGallery = function($items, options) {
	this.$items = $items;
	
	this.options = $.extend(true, {}, this.defaults, options);
	
	this._init();
};

ModalGallery.prototype = {
	defaults: {
		showCount: false,
		loop: false,
		highDpiScaleFactor: 1.5 // How much we will allow the largest image to upscale on high dpi screens
	},
	
	_init: function() {
		var me = this;

		me.$window = $(window);
		me.isBuilt = false;
		me.activeClass = 'modal-gallery-open';
		me.isHighDensity = me._isHighDensity();

		if ( !me.$items.length ) {
			return;
		}

		me.$items.on('click.modal-gallery', function(e) {
			e.preventDefault();
			me.show( me.$items.index( $(this) ) );
		});
	},

	_build: function() {
		var me = this;

		me.carousel = {};
		me.carousel.$el = $('<div class="modal-gallery"/>');
		me.carousel.$container = $('<div class="carousel-container"/>');
		me.carousel.$close = $('<button class="close" aria-label="close"><span>Close</span></button>');
		
		me.carousel.$el.append( me.carousel.$container );
		
		if ( me.options.showCount ) {
			me.carousel.$countContainer = $('<p class="gallery-counts"><span class="count">1</span> / <span class="total">' + me.$items.length + '</span></p>');
			me.carousel.$count = me.carousel.$countContainer.find('.count');
			me.carousel.$total = me.carousel.$countContainer.find('.gallery-counts .total');
			me.carousel.$el.append( me.carousel.$countContainer );
		}
		
		me.carousel.$el.append( me.carousel.$close );
		
		for ( var i = 0; i < me.$items.length; i++ ) {
			me._appendImage(i);
		}
		
		$('body').append( me.carousel.$el );
		
		me.carouselInstance = me.carousel.$container.flickity({
			pageDots: false,
			prevNextButtons: true,
			wrapAround: me.options.loop,
			setGallerySize: false,
			arrowShape: {
				x0: 15,
				x1: 65, y1: 50,
				x2: 70, y2: 45,
				x3: 25
			}
		});

		me.carousel.flkty = me.carouselInstance.data('flickity');

		if ( me.options.showCount ) {
			me.carousel.$total.text( me.carousel.flkty.cells.length );
		}

		me.carouselInstance.on( 'select.flickity', function() {
			me._selectCell( me.carousel.flkty.selectedIndex );
		});

		me.carouselInstance.on( 'staticClick.flickity', function( event, pointer, cellElement, cellIndex ) {
			if ( pointer.target.nodeName !== 'IMG' && pointer.target.nodeName !== 'P' ) {
				// Click is outside of the images and nav, so close modal
				me.close();
			}
		});
	},
	
	_isHighDensity: function(){
		// Test if we are on a 'high density' screen (more than 1.3dppx)
		return ((window.matchMedia && (window.matchMedia('only screen and (min-resolution: 124dpi), only screen and (min-resolution: 1.3dppx), only screen and (min-resolution: 48.8dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3)').matches)) || (window.devicePixelRatio && window.devicePixelRatio > 1.3));
	},
	
	_appendImage: function(i) {
		var me = this;
		
		var imageData = me.$items.eq(i).data('image');
		var $imageContainer = $('<div class="carousel-item"><div class="item-content"><div class="wrapper"><div class="image"><div class="aspect"><div class="loading"></div></div></div></div></div></div>');
		var $img = $('<img alt="" class="lazyload" />');
		
		$img.attr('data-src', imageData.src)
			.attr('width', imageData.width)
			.attr('height', imageData.height);
		
		// Have we got data passed in from sizesArray and srcsArray attributes?
		// If so, use those to build the srcset and sizes data
		if ( imageData.sizesArray && imageData.srcsArray ) {
			var sizesArray = imageData.sizesArray.split(',');
			var srcsArray = imageData.srcsArray.split(',');
			
			if ( sizesArray.length === srcsArray.length ) {
				if ( sizesArray.length > 1 ) {
					// We have more than one image size, so let's build out a srcset & sizes!
					var sizeData = [];
					
					for ( var i = 0; i < sizesArray.length; i++ ) {
						var o = {};
						o.size = parseInt( sizesArray[i].trim() );
						o.src = srcsArray[i].trim();
						sizeData.push(o);
					}
					
					// Let's make sure these are in size order
					sizeData.sort(function(a, b){
						return a.size - b.size;
					});
					
					// The src data should be in order of smallest -> largest, so we know the largest sizes will be last in the array
					var largestSize = sizeData[sizeData.length - 1].size,
						sizes, 
						srcset = "";
						
					for ( var i = 0; i < sizesArray.length; i++ ) {
						if ( i > 0 ) { srcset += ", "; }
						srcset += srcsArray[i].trim() + " " + sizesArray[i].trim() + "w";
					}
					
					if ( me.isHighDensity ) {
						// We are on a high res screen, so use the second-to-last image as the 'largest' size because 
						// the browser will use the largest version to display at that size for the higher dpi
						var secondLargestSize = sizeData[sizeData.length - 2].size;
						
						// Allow a certain amount of upscale for the largest size, afforded by the higher dpi
						var upscaledSize = Math.floor( secondLargestSize * me.options.highDpiScaleFactor );
						// Check that the upscaled size is not larger than the dimensions of the largest available image
						if ( upscaledSize <= largestSize ) {
							largestSize = upscaledSize;
						}
					}
					
					sizes = '(max-width: ' + largestSize + 'px) 100vw, ' + largestSize + 'px';

					$img.attr('data-srcset', srcset)
						.attr('sizes', sizes);
				}
			}
		} else {
			$img.attr('data-srcset', imageData.srcset)
				.attr('sizes', imageData.sizes);
		}

		$imageContainer.find('.aspect').prepend( $img );
		me.carousel.$container.append( $imageContainer );
	},

	_updateForResize: function() {
		var me = this;

		for ( var i = 0; i < me.carousel.flkty.cells.length; i++ ) {
			me._updateImageOrientation( $(me.carousel.flkty.cells[i].element).find('img')[0] );
		}
	},

	_updateImageOrientation: function(img) {
		var $img = $(img);
		var imgAspectRatio = $img.attr('width') / $img.attr('height');
		var $imgContainer = $img.closest('.image');
		var containerAspectRatio = $imgContainer.innerWidth() / $imgContainer.innerHeight();

		if ( imgAspectRatio < containerAspectRatio ) {
			// Portrait
			$img.closest('.carousel-item').attr('data-aspect', 'portrait');
		} else {
			// Landscape
			$img.closest('.carousel-item').attr('data-aspect', 'landscape');
		}
	},

	_selectCell: function(index) {
		var me = this;
		
		if ( me.options.showCount ) {
			me.carousel.$count.text( me.carousel.flkty.selectedIndex + 1 );
		}
	},
	
	show: function(id) {
		var me = this;
		
		if ( typeof(id) === 'undefined' ) {
			var id = 0;
		}

		$('html').addClass( me.activeClass );
		
		if ( !me.isBuilt ) {
			me._build();
			
			me.carouselInstance.flickity( 'select', id, true, true );
			me.carousel.$el.find('.close').on('click', $.proxy( me.close, me ));
			me.isBuilt = true;
		} else {
			me.carouselInstance.flickity('resize');
			me.carouselInstance.flickity( 'select', id, true, true );
		}

		me.refresh();

		// Save a reference to resize listener so that we can remove it later
		me._onWindowResize = me.refresh.bind(this);
		me._onOrientationChange = me.switchOrientation.bind(this);

		// Add window resize listener
		me.$window.on('resize.modal-gallery', me._onWindowResize);
		me.$window.on('orientationchange.modal-gallery', me._onOrientationChange);

		$(document).on('keyup.modal-gallery', function(e) {
			if ( e.keyCode === 27 ) {
				// Esc
				me.close();
			}
			
			if ((e.keyCode || e.which) === 37) {
				// Left arrow
				me.carouselInstance.flickity('previous');
			}
			
			if ((e.keyCode || e.which) === 39) {
				// Right arrow
				me.carouselInstance.flickity('next');
			}
		});
	},

	switchOrientation: function() {
		var me = this;

		me._onWindowResize();
	},

	close: function() {
		var me = this;

		$('html').removeClass( me.activeClass );

		$(document).off('keyup.modal-gallery');
		me.$window.off('resize.modal-gallery', me._onWindowResize);
		me.$window.off('orientationchange.modal-gallery', me._onOrientationChange);
	},

	refresh: function() {
		var me = this;

		me.carouselInstance.flickity('resize');
		me._updateForResize();
	},

	destroy: function() {
		var me = this;

		$('html').removeClass( me.activeClass );

		// Unbind listeners
		$(document).off('keyup.modal-gallery');
		me.$window.off('resize.modal-gallery', me._onWindowResize);
		me.$window.off('orientationchange.modal-gallery', me._onOrientationChange);

		// Kill elements
		if ( me.carouselInstance ) {
		me.carouselInstance.flickity('destroy');
			me.carouselInstance = null;
		me.carousel.$el.remove();
			me.carousel = null;
			me.isBuilt = false;
		}
	}
};
