# Modal Gallery

A javascript component that will take a group of image links and, when clicked, load a [Flickity](https://flickity.metafizzy.co) gallery within a modal overlay. Try out [some examples here](http://code.sneak.co.nz/modal-gallery/).

## Requirements

The plugin requires jQuery to be included in the page, the [Flickity](https://flickity.metafizzy.co) plugin from David Desandro, and the [lazySizes](http://afarkas.github.io/lazysizes/) plugin from Alexander Farkas.

## Options
 
 <table>
 	<tr>
		<th>showCount</th>
		<td>Boolean</td>
		<td>If true, an image count is shown in the modal and will update as you move through the images (e.g. "1/12"). Defaults to false.</td>
	</tr>
	<tr>
		<th>loop</th>
		<td>Boolean</td>
		<td>If true, the Flickity gallery will wrap around. Default is false.</td>
	</tr>
	<tr>
		<th>highDpiScaleFactor</th>
		<td>Number</td>
		<td>
			A scale factor to determine the largest size an image will be displayed at when on a high DPI screen (above 1.3). 
			This is to allow for the fact that on retina screens there may be an acceptable amount of upscaling before image quality degrades too much. 
			Default is 1.5.
		</td>
	</tr>
</table>

## Usage

First include all the required files:

```html
<!-- Required CSS files -->
<link href="vendor/flickity/flickity.css" rel="stylesheet" type="text/css" media="all" />
<link href="dist/modal-gallery.css" rel="stylesheet" type="text/css" media="all" />

<!-- Required JS files -->
<script src="vendor/jquery/jquery.js"></script>
<script src="vendor/flickity/flickity.pkgd.js"></script>
<script src="vendor/lazysizes/lazysizes.js"></script>
<script src="dist/modal-gallery.min.js"></script>
```

The markup requires a set of elements that have a `data-image` attribute containing an object with information about the large version of the image to be loaded in the gallery. The required data is a `src`, `srcset`, `width`, `height`, and the optional `sizes`.

```html
<a href="img/image.jpg" data-image='{ "src": "img/image.jpg", "srcset": "img/image.jpg 1200w, img/image@2x.jpg 2400w", "sizes": "(max-width: 1200px) 100vw, 1200px", "width": 1200, "height": 800 }'>
	<img src="img/image-thumbnail.jpg" />
</a>
```

Instead of passing in 'srcset' and 'sizes' values you can also pass in comma separated lists of sizes and image sources as 'sizesArray' and 'srcsArray' values. Given these values the size and srcset attributes will be automatically generated for the given sizes with retina/high DPI screens in mind. Note that the ordering of these is important, and should match between the two arrays:

```html
<a href="img/image.jpg" data-image='{ "src": "img/image.jpg", "srcsArray": "img/image.jpg, img/image@2x.jpg", "sizesArray": "1200, 2400", "width": 1200, "height": 800 }'>
	<img src="img/image-thumbnail.jpg" />
</a>
```
	
To use the plugin create a new instance and pass in a jQuery array of image links:

```javascript
var carousel = new ModalGallery( $items );
```
	
You can also pass in options for the image count, looping and high DPI scaling behaviour;

```javascript
var carousel = new ModalGallery( $items, {
	showCount: true,
	loop: true,
	highDpiScaleFactor: 1
} );
```

If you may have multiple galleries on a page, you may do something like this:

```javascript
$('.gallery').each(function() {
	var carousel = new ModalGallery( $(this).find('[data-image]'), {
		showCount: false,
		loop: false
	});
});
```

## To-do

- TODO: Make use of lazySizes optional
